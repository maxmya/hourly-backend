package com.dmlabs.hourly

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class HourlyApplication

fun main(args: Array<String>) {
    runApplication<HourlyApplication>(*args)
}

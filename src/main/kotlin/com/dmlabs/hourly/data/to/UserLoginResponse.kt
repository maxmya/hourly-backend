package com.dmlabs.hourly.data.to

data class UserLoginResponse(
    val token: String,
    val username: String,
    val fullName: String,
    val profileUrl: String
)
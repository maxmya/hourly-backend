package com.dmlabs.hourly.data.to

data class UserLoginRequest(
    val username: String,
    val password: String
)
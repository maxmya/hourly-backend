package com.dmlabs.hourly.data.to

import org.springframework.http.HttpStatus

data class ResponseMapper<T>(
    val data: T?,
    val message: String?,
    val success: Boolean,
    val status: HttpStatus
)
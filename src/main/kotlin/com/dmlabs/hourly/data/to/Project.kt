package com.dmlabs.hourly.data.to

import com.dmlabs.hourly.data.entity.ProjectEntity
import com.dmlabs.hourly.data.entity.UserEntity
import java.time.LocalDateTime

data class Project(
    var id: Int,
    var name: String,
    var description: String,
    var leader: User?,
    var createdAt: LocalDateTime?,
    var coverUrl: String?,
    var members: List<User>?
) {
    fun toEntity(): ProjectEntity {
        val members = mutableListOf<UserEntity>()
        this.members?.forEach { user ->
            members.add(user.toEntity())
        }
        return ProjectEntity(
            this.id, this.name,
            this.description, this.leader?.toEntity(),
            this.createdAt, this.coverUrl,
            members
        )
    }
}
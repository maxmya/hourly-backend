package com.dmlabs.hourly.data.to

data class UserRegisterRequest(
    val username:String,
    val password:String,
    val fullName:String,
)
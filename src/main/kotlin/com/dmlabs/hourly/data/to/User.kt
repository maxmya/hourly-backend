package com.dmlabs.hourly.data.to

import com.dmlabs.hourly.data.entity.RoleEntity
import com.dmlabs.hourly.data.entity.TeamEntity
import com.dmlabs.hourly.data.entity.UserEntity

data class User(
    val id: Int,
    val username: String,
    val password: String,
    val fullName: String,
    val profilePicture: String,
    val role: RoleEntity?,
    val teams: List<Team>?
) {
    fun toEntity(): UserEntity {
        val teams = mutableListOf<TeamEntity>()
        this.teams?.forEach { team ->
            teams.add(team.toEntity())
        }
        return UserEntity(
            this.id, this.username, this.password,
            this.fullName, this.profilePicture,
            this.role, teams
        )
    }

}

package com.dmlabs.hourly.data.repository

import com.dmlabs.hourly.data.entity.TeamEntity
import org.springframework.data.jpa.repository.JpaRepository

interface TeamRepository : JpaRepository<TeamEntity,Int> {
}
package com.dmlabs.hourly.data.repository

import com.dmlabs.hourly.data.entity.TaskEntity
import org.springframework.data.jpa.repository.JpaRepository

interface TaskRepository : JpaRepository<TaskEntity, Int> {
}
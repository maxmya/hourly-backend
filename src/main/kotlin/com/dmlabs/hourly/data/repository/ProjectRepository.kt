package com.dmlabs.hourly.data.repository

import com.dmlabs.hourly.data.entity.ProjectEntity
import org.springframework.data.jpa.repository.JpaRepository

interface ProjectRepository : JpaRepository<ProjectEntity,Int> {
}
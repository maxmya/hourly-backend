package com.dmlabs.hourly.data.repository

import com.dmlabs.hourly.data.entity.RoleEntity
import com.dmlabs.hourly.data.entity.UserEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : JpaRepository<UserEntity, Int> {

    fun existsByUsername(username: String): Boolean
    fun findUserByUsername(username: String): UserEntity
    fun findAllByRole(role: RoleEntity): List<UserEntity>

}
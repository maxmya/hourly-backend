package com.dmlabs.hourly.data.repository

import com.dmlabs.hourly.data.entity.RoleEntity
import org.springframework.data.jpa.repository.JpaRepository

interface RoleRepository : JpaRepository<RoleEntity, Int> {
}
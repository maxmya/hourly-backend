package com.dmlabs.hourly.data.entity

import javax.persistence.*


@Entity
@Table(name = "roles", catalog = "hourly", schema = "public")
data class RoleEntity(
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = 0,
    val name: String
)
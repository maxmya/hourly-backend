package com.dmlabs.hourly.data.entity

import com.dmlabs.hourly.data.to.Project
import com.dmlabs.hourly.data.to.User
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "project", catalog = "hourly", schema = "public")
data class ProjectEntity(
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = 0,
    val name: String,
    val description: String,
    @OneToOne
    val leader: UserEntity?,
    @Column(name = "created_at")
    val createdAt: LocalDateTime?,
    @Column(name = "cover_url")
    val coverUrl: String?,
    @ManyToMany(cascade = [CascadeType.ALL])
    @JoinTable(
        name = "user_projects",
        joinColumns = [JoinColumn(name = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "project_id")]
    )
    val members: List<UserEntity>?
) {
    fun fromEntity(): Project {
        val members = mutableListOf<User>()
        this.members?.forEach { userEntity ->
            members.add(userEntity.fromEntity())
        }
        return Project(
            this.id, this.name, this.description,
            this.leader?.fromEntity(), this.createdAt,
            this.coverUrl, members
        )
    }
}
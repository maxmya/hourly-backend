package com.dmlabs.hourly.data.entity

import javax.persistence.*

@Entity
@Table(name = "task", catalog = "hourly", schema = "public")
data class TaskEntity(
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = 0,
    val name: String,
    val description: String,
    @OneToOne
    val project: ProjectEntity,
    @OneToOne
    @JoinColumn(name = "assignee_id", referencedColumnName = "id")
    val assignee: UserEntity,
    @OneToOne
    @JoinColumn(name = "creator_id", referencedColumnName = "id")
    val creator: UserEntity
)
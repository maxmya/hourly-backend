package com.dmlabs.hourly.data.entity

import com.dmlabs.hourly.data.to.Team
import com.dmlabs.hourly.data.to.User
import javax.persistence.*


@Entity
@Table(name = "user", catalog = "hourly", schema = "public")
data class UserEntity(
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = 0,
    val username: String,
    val password: String,
    @Column(name = "full_name")
    val fullName: String,
    @Column(name = "picture_url")
    val pictureUrl: String,
    @OneToOne
    val role: RoleEntity?,
    @ManyToMany(mappedBy = "members")
    val teams: List<TeamEntity>?
) {
    fun fromEntity(): User {
        val teams = mutableListOf<Team>()
        this.teams?.forEach { teamEntity ->
            teams.add(teamEntity.fromEntity())
        }
        return User(
            this.id, this.username, this.password, this.fullName,
            this.pictureUrl, this.role, teams
        )
    }
}

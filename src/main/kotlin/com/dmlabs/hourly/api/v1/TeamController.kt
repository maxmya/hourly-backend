package com.dmlabs.hourly.api.v1

import com.dmlabs.hourly.data.to.Team
import com.dmlabs.hourly.service.FilesStorageService
import com.dmlabs.hourly.service.TeamService
import com.dmlabs.hourly.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.support.ServletUriComponentsBuilder

@CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
@RestController
@RequestMapping("/api/team")
class TeamController
@Autowired
constructor(
    val teamService: TeamService,
    val userService: UserService,
    val filesStorageService: FilesStorageService
) {


    @PostMapping("/add", consumes = ["multipart/form-data"])
    fun addTeam(
        @RequestPart(name = "file") file: MultipartFile,
        @RequestPart(name = "body") team: Team
    ): ResponseEntity<*> {
        val imgUrl = filesStorageService.storeFile(file)

        val fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
            .path("/storage/downloadFile/")
            .path(imgUrl)
            .toUriString()

        val response = teamService.addTeam(team, fileDownloadUri)
        return ResponseEntity(response, response.status)
    }

    @GetMapping("/admins")
    fun listPossibleLeaders(): ResponseEntity<*> {
        val response = userService.getAllAdmins()
        return ResponseEntity(response, response.status)
    }

    @DeleteMapping("/delete/{id}")
    fun deleteTeam(@PathVariable("id") id: Int): ResponseEntity<*> {
        val response = teamService.deleteTeam(id)
        return ResponseEntity(response, response.status)
    }

    @GetMapping("/list")
    fun listTeams(): ResponseEntity<*> {
        val list = teamService.listAllTeams()
        return ResponseEntity(list, list.status)
    }


}
package com.dmlabs.hourly.api.v1

import com.dmlabs.hourly.data.to.Project
import com.dmlabs.hourly.service.FilesStorageService
import com.dmlabs.hourly.service.ProjectService
import com.dmlabs.hourly.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.support.ServletUriComponentsBuilder

@CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
@RestController
@RequestMapping("/api/project")
class ProjectController
@Autowired
constructor(
    val projectService: ProjectService,
    val userService: UserService,
    val filesStorageService: FilesStorageService
) {


    @PostMapping("/add", consumes = ["multipart/form-data"])
    fun addProject(
        @RequestPart(name = "file") file: MultipartFile,
        @RequestPart(name = "body") project: Project
    ): ResponseEntity<*> {
        val imgUrl = filesStorageService.storeFile(file)

        val fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
            .path("/storage/downloadFile/")
            .path(imgUrl)
            .toUriString()

        val response = projectService.addProject(project, fileDownloadUri)
        return ResponseEntity(response, response.status)
    }


    @DeleteMapping("/delete/{id}")
    fun deleteProject(@PathVariable("id") id: Int): ResponseEntity<*> {
        val response = projectService.deleteProject(id)
        return ResponseEntity(response, response.status)
    }

    @GetMapping("/list")
    fun listProjects(): ResponseEntity<*> {
        val list = projectService.listAllProjects()
        return ResponseEntity(list, list.status)
    }


}
package com.dmlabs.hourly.api.v1

import com.dmlabs.hourly.service.ReporterService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest

@CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
@RestController
@RequestMapping("/api/role")
class RoleController
@Autowired
constructor(val reporterService: ReporterService) {

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/admin")
    fun validateToken(request: HttpServletRequest): ResponseEntity<*>? {
        return ResponseEntity(reporterService.reportSuccess(), HttpStatus.OK)
    }

}
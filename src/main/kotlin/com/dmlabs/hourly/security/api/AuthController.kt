package com.dmlabs.hourly.security.api

import com.dmlabs.hourly.data.to.UserLoginRequest
import com.dmlabs.hourly.data.to.UserRegisterRequest
import com.dmlabs.hourly.security.service.AuthService
import com.dmlabs.hourly.service.FilesStorageService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import com.dmlabs.hourly.data.to.ResponseMapper

import org.springframework.http.HttpStatus

import javax.servlet.http.HttpServletRequest

import org.springframework.web.bind.annotation.GetMapping


@CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
@RestController
@RequestMapping("/api/auth")
class AuthController
@Autowired
constructor(
    val authService: AuthService,
    val filesStorageService: FilesStorageService
) {

    @PostMapping("/register")
    fun registerNewUser(
        @RequestPart(name = "file") file: MultipartFile,
        @RequestPart(name = "body") userRegisterRequest: UserRegisterRequest
    ): ResponseEntity<*> {
        val imgUrl = filesStorageService.storeFile(file)

        val fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
            .path("/storage/downloadFile/")
            .path(imgUrl)
            .toUriString()

        val response = authService.registerUser(userRegisterRequest, fileDownloadUri)
        return ResponseEntity(response, response.status)
    }

    @PostMapping("/login")
    fun loginUser(@RequestBody userLoginRequest: UserLoginRequest): ResponseEntity<*> {
        val response = authService.loginUser(userLoginRequest);
        return ResponseEntity(response, response.status)
    }

    @GetMapping("/validate")
    fun validateToken(request: HttpServletRequest): ResponseEntity<*>? {
        val authorizationHeader = request.getHeader("Authorization")
            ?: return ResponseEntity(false, HttpStatus.UNAUTHORIZED)
        val response: ResponseMapper<*> = authService.validateToken(authorizationHeader)
        return ResponseEntity(response, response.status)
    }


}
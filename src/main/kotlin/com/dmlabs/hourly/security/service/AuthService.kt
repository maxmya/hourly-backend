package com.dmlabs.hourly.security.service

import com.dmlabs.hourly.data.entity.UserEntity
import com.dmlabs.hourly.data.repository.RoleRepository
import com.dmlabs.hourly.data.repository.UserRepository
import com.dmlabs.hourly.data.to.ResponseMapper
import com.dmlabs.hourly.data.to.UserLoginRequest
import com.dmlabs.hourly.data.to.UserLoginResponse
import com.dmlabs.hourly.data.to.UserRegisterRequest
import com.dmlabs.hourly.security.JwtProvider
import com.dmlabs.hourly.service.ReporterService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.lang.Exception

@Service
class AuthService @Autowired constructor(
    val userRepository: UserRepository,
    val roleRepository: RoleRepository,
    val reporterService: ReporterService,
    val passwordEncoder: PasswordEncoder,
    val authenticationManager: AuthenticationManager,
    val jwtProvider: JwtProvider
) {


    fun registerUser(
        userRegisterRequest: UserRegisterRequest,
        userProfilePictureUrl: String
    ): ResponseMapper<Boolean> {
        return try {

            if (userRepository.existsByUsername(userRegisterRequest.username)) {
                return reporterService.reportError("user name already existed")
            }

            val normalRole = roleRepository.getOne(0)

            val user = UserEntity(
                username = userRegisterRequest.username,
                password = passwordEncoder.encode(userRegisterRequest.password),
                fullName = userRegisterRequest.fullName,
                pictureUrl = userProfilePictureUrl,
                role = normalRole, teams = null
            )
            userRepository.save(user)
            reporterService.reportSuccess()
        } catch (e: Exception) {
            reporterService.reportError(e)
        }
    }

    fun loginUser(
        userLoginRequest: UserLoginRequest
    ): ResponseMapper<UserLoginResponse> {
        return try {
            val authentication = authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(
                    userLoginRequest.username,
                    userLoginRequest.password
                )
            )
            SecurityContextHolder.getContext().authentication = authentication
            val token = jwtProvider.generateJwtToken(authentication)
            val userData = userRepository.findUserByUsername(userLoginRequest.username)
            val response = UserLoginResponse(token, userData.username, userData.fullName, userData.pictureUrl)
            reporterService.reportSuccess(response)
        } catch (e: Exception) {
            reporterService.reportError(e)
        }
    }

    fun validateToken(authHeader: String): ResponseMapper<Boolean> {
        return try {
            val token = authHeader.replace("Bearer ", "")
            if (token.isEmpty()) {
                return reporterService.reportSuccess(false)
            }
            jwtProvider.validateJwtToken(token)
            reporterService.reportSuccess()
        } catch (e: Exception) {
            reporterService.reportError(e)
        }
    }

}
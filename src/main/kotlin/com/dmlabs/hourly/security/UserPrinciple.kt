package com.dmlabs.hourly.security

import com.dmlabs.hourly.data.entity.UserEntity
import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails


class UserPrinciple(val user: UserEntity) : UserDetails {

    private var myCustomAuthorities: MutableCollection<out GrantedAuthority>


    init {
        val mAuth: MutableCollection<GrantedAuthority> = mutableListOf()
        val role = user.role?.name // each user have only one role
        mAuth.add(SimpleGrantedAuthority("ROLE_$role"))
        myCustomAuthorities = mAuth
    }


    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return myCustomAuthorities
    }

    override fun isEnabled(): Boolean {
        return true
    }

    override fun getUsername(): String {
        return user.username
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    @JsonIgnore
    override fun getPassword(): String {
        return user.password
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }
}
package com.dmlabs.hourly.service

import com.dmlabs.hourly.data.to.ResponseMapper
import org.springframework.stereotype.Service
import org.springframework.http.HttpStatus

import org.springframework.dao.DataIntegrityViolationException


@Service
class ReporterService {

    fun <T> reportError(exception: Exception, data: T): ResponseMapper<T> {
        exception.printStackTrace()
        var errorMessage = if (exception.message != null) exception.message else "ERROR"
        if (exception is DataIntegrityViolationException) {
            errorMessage = "Cannot modify this entity as it's used in other relations!"
        }
        return ResponseMapper(data, errorMessage, false, HttpStatus.INTERNAL_SERVER_ERROR)
    }

    fun <T> reportError(exception: Exception): ResponseMapper<T> {
        exception.printStackTrace()
        var errorMessage = if (exception.message != null) exception.message else "ERROR"
        if (exception is DataIntegrityViolationException) {
            errorMessage = "Cannot modify this entity as it's used in other relations!"
        }
        return ResponseMapper(null, errorMessage, false, HttpStatus.INTERNAL_SERVER_ERROR)
    }

    fun <T> reportError(message: String): ResponseMapper<T> {
        return ResponseMapper(null, message, false, HttpStatus.BAD_REQUEST)
    }

    fun <T> reportSuccess(data: T): ResponseMapper<T> {
        return ResponseMapper(data, "done", true, HttpStatus.OK)
    }

    fun reportSuccess(): ResponseMapper<Boolean> {
        return ResponseMapper(true, "done", true, HttpStatus.OK)
    }


}
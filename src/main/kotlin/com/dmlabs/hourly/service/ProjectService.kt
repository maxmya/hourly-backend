package com.dmlabs.hourly.service

import com.dmlabs.hourly.data.repository.ProjectRepository
import com.dmlabs.hourly.data.to.ResponseMapper
import com.dmlabs.hourly.data.to.Project
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class ProjectService
@Autowired
constructor(
    val projectRepository: ProjectRepository,
    val reporterService: ReporterService
) {

    fun addProject(project: Project, coverUrl: String): ResponseMapper<Boolean> {
        return try {
            project.createdAt = LocalDateTime.now()
            project.coverUrl = coverUrl

            projectRepository.save(project.toEntity())
            reporterService.reportSuccess()
        } catch (e: Exception) {
            reporterService.reportError(e)
        }
    }

    fun listAllProjects(): ResponseMapper<List<Project>> {
        return try {
            val projects = mutableListOf<Project>()
            projectRepository.findAll().forEach { projectEntity -> projects.add(projectEntity.fromEntity()) }
            reporterService.reportSuccess(projects)
        } catch (e: Exception) {
            reporterService.reportError(e)
        }
    }

    fun deleteProject(id: Int): ResponseMapper<Boolean> {
        return try {
            projectRepository.deleteById(id)
            reporterService.reportSuccess()
        } catch (e: Exception) {
            reporterService.reportError(e)
        }
    }


}
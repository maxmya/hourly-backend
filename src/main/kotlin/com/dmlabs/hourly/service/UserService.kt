package com.dmlabs.hourly.service

import com.dmlabs.hourly.data.repository.RoleRepository
import com.dmlabs.hourly.data.repository.UserRepository
import com.dmlabs.hourly.data.to.ResponseMapper
import com.dmlabs.hourly.data.to.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserService
@Autowired
constructor(
    val reporterService: ReporterService,
    val userRepository: UserRepository,
    val roleRepository: RoleRepository
) {

    fun getAllAdmins(): ResponseMapper<List<User>> {
        return try {
            val admin = roleRepository.getOne(1)
            val listOfAdmins = userRepository.findAllByRole(admin)
            val adminsTo = mutableListOf<User>()
            listOfAdmins.forEach { admin ->
                adminsTo.add(admin.fromEntity())
            }
            reporterService.reportSuccess(adminsTo)
        } catch (e: Exception) {
            reporterService.reportError(e)
        }
    }

}
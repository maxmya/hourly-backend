package com.dmlabs.hourly.service

import com.dmlabs.hourly.data.repository.TeamRepository
import com.dmlabs.hourly.data.to.ResponseMapper
import com.dmlabs.hourly.data.to.Team
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class TeamService
@Autowired
constructor(
    val teamRepository: TeamRepository,
    val reporterService: ReporterService
) {

    fun addTeam(team: Team, coverUrl: String): ResponseMapper<Boolean> {
        return try {
            team.createdAt = LocalDateTime.now()
            team.coverUrl = coverUrl

            teamRepository.save(team.toEntity())
            reporterService.reportSuccess()
        } catch (e: Exception) {
            reporterService.reportError(e)
        }
    }

    fun listAllTeams(): ResponseMapper<List<Team>> {
        return try {
            val teams = mutableListOf<Team>()
            teamRepository.findAll().forEach { teamEntity -> teams.add(teamEntity.fromEntity()) }
            reporterService.reportSuccess(teams)
        } catch (e: Exception) {
            reporterService.reportError(e)
        }
    }

    fun deleteTeam(id: Int): ResponseMapper<Boolean> {
        return try {
            teamRepository.deleteById(id)
            reporterService.reportSuccess()
        } catch (e: Exception) {
            reporterService.reportError(e)
        }
    }


}